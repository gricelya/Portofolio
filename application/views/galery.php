<div class="container light">
  <div class="content">
    <div class="title icon-preview">
      <h1>Galery <span>This is My Galery</span></h1>
      <div class="description">
        <nav id="filter"></nav>
      </div>
    </div>
    <article class="portfolio">
      <ul id="stage">
      <li data-tags="Mountain">
          <div class="thumb"><a rel="lightbox" href="<?php echo base_url()?>/Assets/images/sample/1.jpg"><img src="<?php echo base_url()?>/Assets/images/sample/1.jpg" alt=""></a></div>
          <div class="info">
            <h3>Best Frend</h3>
            <p>Kawah Ijen</p>
          </div>
        </li>
        <li data-tags="Photography">
          <div class="thumb"><a rel="lightbox" href="<?php echo base_url()?>/Assets/images/sample/1.jpg"><img src="<?php echo base_url()?>/Assets/images/sample/2.jpg" alt=""></a></div>
          <div class="info">
            <h3>Stand Alone</h3>
            <p>Strong Women</p>
          </div>
        </li>
        <li data-tags="Beach">
          <div class="thumb"><a rel="lightbox" href="<?php echo base_url()?>/Assets/images/sample/1.jpg"><img src="<?php echo base_url()?>/Assets/images/sample/3.jpg" alt=""></a></div>
          <div class="info">
            <h3>The Wild One</h3>
            <p>Illustration</p>
          </div>
        </li>
        <li data-tags="Sky">
          <div class="thumb"><a rel="lightbox" href="<?php echo base_url()?>/Assets/images/sample/1.jpg"><img src="<?php echo base_url()?>/Assets/images/sample/4.jpg" alt=""></a></div>
          <div class="info">
            <h3>Pure Beach</h3>
            <p>Boom Beach</p>
          </div>
        </li>
        <li data-tags="Design">
          <div class="thumb"><a rel="lightbox" href="<?php echo base_url()?>/Assets/images/sample/1.jpg"><img src="<?php echo base_url()?>/Assets/images/sample/5.jpg" alt=""></a></div>
          <div class="info">
            <h3>Culture</h3>
            <p>Dayak Mine</p>
          </div>
        </li>
        <li data-tags="Culture">
          <div class="thumb"><a rel="lightbox" href="<?php echo base_url()?>/Assets/images/sample/1.jpg"><img src="<?php echo base_url()?>/Assets/images/sample/6.jpg" alt=""></a></div>
          <div class="info">
            <h3>Pumkin</h3>
            <p>JatimPark</p>
          </div>
        </li>
      </ul>
    </article>
  </div>
</div>