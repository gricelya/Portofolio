<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<title>Merry Gricelya</title>
<meta charset="UTF-8">
<!--Style Sheet-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/Assets/css/flexslider.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/Assets/css/sequence.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/Assets/css/lightbox.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/Assets/css/style.css" media="all">
<!--Special Styles-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/Assets/css/style.css" title="default" media="screen">
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>/Assets/css/elegant.css" title="elegant" media="screen">
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>/<?php echo base_url()?>/Assets/css/modern.css" title="modern" media="screen">
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>/Assets/css/colorfull.css" title="colorfull" media="screen">
<!--Responsive-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/Assets/css/responsitive.css" media="all">
<!--Google fonts-->
<link href='http://fonts.googleapis.com/css?family=Russo+One&subset=latin,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
<!--Javascript-->
<script src="<?php echo base_url()?>/Assets/js/jquery-1.7.2.min.js"></script>
<!--[if lt IE9]>
<script src="js/html5.js"></script>
<script src="js/IE7.js"></script>
<![endif]-->
</head>
<body>
<header class="dark">
  <nav>
    <div id="logo">
      <h1>Merry</h1>
    </div>
    <div id="menu">
      <ul>
        <li> <a href="<?php echo base_url()?>index.php/Welcome" >Home</a></li>
        <li> <a href="<?php echo base_url()?>index.php/Welcome/portofolio">Portofolio</a></li> 
        <li> <a href="<?php echo base_url()?>index.php/Welcome/galery">Galery</a> </li>
        <li> <a href="<?php echo base_url()?>index.php/Welcome/About">About</a></li>
        <li> <a href="<?php echo base_url()?>index.php/Welcome/Contact">Contact</a> </li>
      </ul>
    </div>
  </nav>
</header>
<section id="slider">
  <div id="sequence-preloader">
    <div class="preloading"> <img src="<?php echo base_url()?>/Assets/images/icons/loader.gif" alt=""> </div>
  </div>
  <div id="slideshow"> <img class="prev" src="<?php echo base_url()?>/Assets/images/prev.png" alt=""> <img class="next" src="<?php echo base_url()?>/Assets/images/next.png" alt="">
    <ul>
      <li> <img class="backgrd" src="<?php echo base_url()?>/Assets/images/bg/2.jpg" alt=""> 
      <li> <img class="backgrd" src="<?php echo base_url()?>/Assets/images/bg/1.jpg" alt=""> 
      <li> <img class="backgrd" src="<?php echo base_url()?>/Assets/images/bg/3.jpg" alt="">
      <li> <img class="backgrd" src="<?php echo base_url()?>/Assets/images/bg/4.jpg" alt="">
      </li>
    </ul>
  </div>
</section>
<?php $this->load->view($content);?>
<!--Javascript-->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="<?php echo base_url()?>/Assets/js/jquery.flexslider-min.js"></script>
<script src="<?php echo base_url()?>/Assets/js/sequence.jquery-min.js"></script>
<script src="<?php echo base_url()?>/Assets/js/waypoints.min.js"></script>
<script src="<?php echo base_url()?>/Assets/js/quicksand.js"></script>
<script src="<?php echo base_url()?>/Assets/js/jquery.masonery.js"></script>
<script src="<?php echo base_url()?>/Assets/js/modernizr.custom.js"></script>
<script src="<?php echo base_url()?>/Assets/js/gmaps.js"></script>
<script src="<?php echo base_url()?>/Assets/js/jquery.accordion.js"></script>
<script src="<?php echo base_url()?>/Assets/js/lightbox.js"></script>
<script src="<?php echo base_url()?>/Assets/js/styleswitch.js"></script>
<script src="<?php echo base_url()?>/Assets/js/custom.js"></script>
<!--Sequence slider-->
<script>
var options = {
    autoPlay: true,
    nextButton: ".next",
    prevButton: ".prev",
    preloader: "#sequence-preloader",
    prependPreloader: false,
    prependPreloadingComplete: "#sequence-preloader, #slideshow",
    animateStartingFrameIn: true,
    transitionThreshold: 500,
    nextButtonAlt: " ",
    prevButtonAlt: " ",
    afterPreload: function () {
        $(".prev, .next").fadeIn(500);
        if (!slideShow.transitionsSupported) {
            $("#slideshow").animate({
                "opacity": "1"
            }, 1000);
        }
    }
};
var slideShow = $("#slideshow").sequence(options).data("sequence");
if (!slideShow.transitionsSupported || slideShow.prefix == "-o-") {
    $("#slideshow").animate({
        "opacity": "1"
    }, 1000);
    slideShow.preloaderFallback();
}
</script>
<script>
new GMaps({
    div: '#map',
    lat: 44.79913,
    lng: 20.47662
});
</script>
<script>
if (typeof jQuery == 'undefined') {
    document.write(unescape("%3Cscript src='<?php echo base_url()?>/Assets/js/jquery.js'%3E%3C/script%3E"));
}
</script>
</body>
</html>
