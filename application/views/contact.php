<div class="container light">
  <div class="content">
    <div class="title icon-phone">
      <h1>Contact <span>What we offer</span></h1>
      <div class="description"> <a href=""><img src="<?php echo base_url()?>/Assets/images/icons/instagram.png" alt="" class="social"></a> <a href="#"><img src="<?php echo base_url()?>/Assets/images/icons/fb.png" alt="" class="social"></a> <a href="#"><img src="<?php echo base_url()?>/Assets/images/icons/tw.jpg" alt="" class="social"></a></div>
    </div>
    <div class="full">
      <div id="contact-form">
        <form id="contact-us" action="#">
          <div class="column-half">
            <div class="formblock">
              <input type="text" name="contactName" id="contactName" value="" class="txt requiredField" placeholder="Name:">
            </div>
            <div class="formblock">
              <input type="text" name="email" id="email" value="" class="txt requiredField email" placeholder="Email:">
            </div>
            <div class="formblock">
              <input type="text" name="website" id="website" value="" class="txt website" placeholder="Website:">
            </div>
            <div class="formblock">
              <textarea name="comments" id="commentsText" class="txtarea requiredField" placeholder="Message:"></textarea>
            </div>
            <button name="submit" type="submit" class="subbutton"></button>
          </div>
          <div class="column-half last">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>